<?php 

/*
S02 - Selection Control Structures and Array Manipulation
Activity Instructions: 

1. Create code.php and index.php inside the a1 folder.
2. In code.php, create a function named printDivisibleOfFive that will perform the following:
 - Using loops, print all numbers that are divisible by 5 from 0 to 1000.
 - Stop the loop when the loop reaches its 100th iteration.
 - Invoke the function in the index.php
3. In code.php again, create an empty array named "students".
4. In index.php, perform the following using array functions:
 - Accept a name of the student and add it to the "students" array.
 - Print the names added so far in the "students" array.
 - Count the number of names in the "students" array.
 - Add another student then print the array and its new count.
 - Finally, remove the firs student and print the array and its count.
*/

// 2. In code.php, create a function named printDivisibleOfFive that will perform the following:
 // - Using loops, print all numbers that are divisible by 5 from 0 to 1000.
 // - Stop the loop when the loop reaches its 100th iteration.
 // - Invoke the function in the index.php

 function printDivisibleOfFive(){
 	for($count=0, $iteration=0; $count<=1000;$count++){
		if($count % 5 === 0){
			echo $count . " ";
			$iteration++;
			
		}
		
		if ($count > 100){
			break;
		}
	}
}
 
 // 3. In code.php again, create an empty array named "students".

$students = [];




 


