<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S02: Activity</title>
</head>
<body>
	<?php printDivisibleOfFive(); ?>

	<!-- 
4. In index.php, perform the following using array functions:
 - Accept a name of the student and add it to the "students" array.
 - Print the names added so far in the "students" array.
 - Count the number of names in the "students" array.
 - Add another student then print the array and its new count.
 - Finally, remove the firs student and print the array and its count.
	 -->
	 <?php array_push($students, 'Adriana Lima'); ?>
	 <pre><?php var_dump($students); ?></pre>
	 <pre>Array Count: <?php echo count($students); ?></pre>
	 <?php array_push($students, 'Miranda Kerr'); ?>

	 <pre><?php var_dump($students); ?></pre>
	 <pre>Array Count: <?php echo count($students); ?></pre>

	 <?php array_shift($students); ?>
	 <pre><?php var_dump($students); ?></pre>
	 <pre>Array Count: <?php echo count($students); ?></pre>



</body>
</html>